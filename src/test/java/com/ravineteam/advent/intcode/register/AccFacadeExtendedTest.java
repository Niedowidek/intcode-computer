package com.ravineteam.advent.intcode.register;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class AccFacadeExtendedTest {

    @Test
    public void shouldMultiply() {
        AccFacade.getInstance().lda(BigInteger.valueOf(99));
        AccFacadeExtended.getInstance().multiply(BigInteger.valueOf(99));
        assertEquals(BigInteger.valueOf(9801), Accumulator.getInstance().read());
    }

    @Test
    public void shouldMultiply2() {
        AccFacade.getInstance().lda(BigInteger.valueOf(3));
        AccFacadeExtended.getInstance().multiply(BigInteger.valueOf(2));
        assertEquals(BigInteger.valueOf(6), Accumulator.getInstance().read());
    }
}