package com.ravineteam.advent.intcode.register;

import com.ravineteam.advent.intcode.branch.Invokable;

public class Jump {
    private static Jump ourInstance = new Jump();

    public static Jump getInstance() {
        return ourInstance;
    }

    private Jump() {
    }

    public void jmp(Invokable invokable) {
        if (invokable != null) {
            invokable.invoke();
        }
    }
}
