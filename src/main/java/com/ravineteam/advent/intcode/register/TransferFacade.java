package com.ravineteam.advent.intcode.register;

public class TransferFacade {
    private static TransferFacade ourInstance = new TransferFacade();

    public static TransferFacade getInstance() {
        return ourInstance;
    }

    private TransferFacade() {
    }

    public void tax() {
        XRegister.getInstance().write(Accumulator.getInstance().read());
    }

    public void tay() {
        YRegister.getInstance().write(Accumulator.getInstance().read());
    }

    public void txa() {
        Accumulator.getInstance().loadValue(XRegister.getInstance().read());
    }

    public void tya() {
        Accumulator.getInstance().loadValue(YRegister.getInstance().read());
    }

}
