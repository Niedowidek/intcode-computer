package com.ravineteam.advent.intcode.register;

public class Flags {
    private static Flags ourInstance = new Flags();

    public static final short CARRY = 0;
    public static final short ZERO = 1;
    public static final short INTERRUPT = 2;
    public static final short DECIMAL = 3;
    public static final short BREAK = 4;
    public static final short UNUSED = 5;
    public static final short OVERFLOW = 6;
    public static final short SIGN = 7;

    private boolean[] flags = new boolean[] {false, false,false,false,false,false,false,false};

    public static Flags getInstance() {
        return ourInstance;
    }

    private Flags() {
    }

    void set(short flag) {
        if (flag >= 0 && flag <= 7) {
            flags[flag] = true;
        }
    }

    void unset(short flag) {
        if (flag >= 0 && flag <= 7) {
            flags[flag] = false;
        }
    }


    Boolean read(short flag) {
        if (flag >= 0 && flag <= 7) {
            return flags[flag];
        }
        return null;
    }

}
