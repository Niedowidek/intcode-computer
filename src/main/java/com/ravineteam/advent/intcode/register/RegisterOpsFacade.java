package com.ravineteam.advent.intcode.register;

import java.math.BigInteger;

public class RegisterOpsFacade {
    private static RegisterOpsFacade ourInstance = new RegisterOpsFacade();

    public static RegisterOpsFacade getInstance() {
        return ourInstance;
    }

    private RegisterOpsFacade() {
    }

    public void inx() {
        XRegister.getInstance().increase();
    }

    public void dex() {
        XRegister.getInstance().decrease();
    }

    public void iny() {
        YRegister.getInstance().increase();
    }

    public void dey() {
        YRegister.getInstance().decrease();
    }

    public void stx(int address) { XRegister.getInstance().storeValue(address);}

    public void sty(int address) { YRegister.getInstance().storeValue(address);}

    public void ldx(int address) {
        XRegister.getInstance().loadValue(address);
    }

    public void ldx(int address, Registers register) {
        XRegister.getInstance().loadValue(address, register);
    }

    public void ldx(BigInteger arg) {
        XRegister.getInstance().write(arg);
    }

    public void ldy(int address) {
        YRegister.getInstance().loadValue(address);
    }

    public void ldy(int address, Registers register) {
        YRegister.getInstance().loadValue(address, register);
    }

    public void ldy(BigInteger arg) {
        YRegister.getInstance().write(arg);
    }


}
