package com.ravineteam.advent.intcode.register;

import java.math.BigInteger;
import java.util.BitSet;


public class AccFacade {
    private static AccFacade ourInstance = new AccFacade();

    public static AccFacade getInstance() {
        return ourInstance;
    }

    private AccFacade() {
    }

    public void lda(BigInteger arg) {
        Accumulator.getInstance().loadValue(arg);
    }

    public void lda(int address) {
        Accumulator.getInstance().loadValue(address);
    }

    public void lda(int address, Registers register) {
        Accumulator.getInstance().loadValue(address, register);
    }

    public void sta(int address) {
        Accumulator.getInstance().storeValue(address);
    }

    public void sta(int address, Registers register) {
        Accumulator.getInstance().storeValue(address, register);
    }

    public void cmp(BigInteger arg) {
        Accumulator.getInstance().compare(arg);
    }

    public void adc(BigInteger arg) {
        Accumulator.getInstance().add(arg);
    }

    public void adc(int address) {
        Accumulator.getInstance().add(address);
    }
}
