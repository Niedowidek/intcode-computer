package com.ravineteam.advent.intcode.register;

import com.ravineteam.advent.intcode.branch.Invokable;
import org.apache.commons.lang3.BooleanUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Branch {
    private static Branch ourInstance = new Branch();

    public static Branch getInstance() {
        return ourInstance;
    }

    private Branch() {
    }

    public void equal(Invokable invokable) {
        boolean equal = BooleanUtils.toBoolean(Flags.getInstance().read(Flags.ZERO));
        if (equal) {
            invokable.invoke();
        }
    }

    public void notEqual(Invokable invokable) {
        boolean equal = BooleanUtils.toBoolean(Flags.getInstance().read(Flags.ZERO));
        if ( ! equal ) {
            invokable.invoke();
        }
    }

}
