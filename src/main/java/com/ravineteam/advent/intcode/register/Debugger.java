package com.ravineteam.advent.intcode.register;

public class Debugger {

    private Accumulator accumulator = Accumulator.getInstance();
    private XRegister xRegister = XRegister.getInstance();
    private YRegister yRegister = YRegister.getInstance();
    private Flags flags = Flags.getInstance();
    private Memory memory = Memory.getInstance();

    public String printRegisters() {
        return
                "A\tX\tY\n" +
                        Accumulator.getInstance().read() + "\t" +
                        XRegister.getInstance().read() + "\t" +
                        YRegister.getInstance().read() + "\t";
    }

    public String printMemory(int address) {
        return
                Memory.getInstance().read(address).toString();
    }

    public String printMemoryXIndexed(int address) {
        return
                Memory.getInstance().read(address + XRegister.getInstance().read().intValue()).toString();
    }

}
