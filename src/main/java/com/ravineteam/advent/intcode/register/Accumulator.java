package com.ravineteam.advent.intcode.register;

import java.math.BigInteger;

public class Accumulator {
    private static Accumulator ourInstance = new Accumulator();
    private BigInteger value;

    static Accumulator getInstance() {
        return ourInstance;
    }

    private Accumulator() {
        value = BigInteger.ZERO;
    }

    void add(BigInteger arg) {
        if (arg != null) {
            this.value = this.value.add(arg);
        }
    }

    void add(int address) {
        if (address >= 0) {
            this.value = this.value.add(Memory.getInstance().read(address));
        }
    }

    void substract(BigInteger arg) {
        if (arg != null) {
            this.value = this.value.subtract(arg);
        }
    }

    void substract(int address) {
        if (address >= 0) {
            this.value = this.value.subtract(Memory.getInstance().read(address));
        }
    }

    void loadValue(BigInteger arg) {
        if (arg != null) {
            this.value = BigInteger.ZERO.add(arg);
        }
    }

    void loadValue(int address) {
        if (address >= 0) {
            this.value = Memory.getInstance().read(address);
        }
    }

    void loadValue(int address, int offset) {
        if (address >= 0 && offset >= 0) {
            this.value = Memory.getInstance().read(address + offset);
        }
    }

    void loadValue(int address, Registers register) {
        if (address >= 0 && register != null) {
            Register r;
            if (register.equals(Registers.X)) {
                r = XRegister.getInstance();
            } else {
                r = YRegister.getInstance();
            }
            this.value = BigInteger.ZERO.add(Memory.getInstance().read(address + r.read().intValue()));
        }
    }

    void compare(BigInteger arg) {
        if (arg != null) {
            if (read().equals(arg)) {
                Flags.getInstance().set(Flags.ZERO);
            } else if (read().compareTo(arg) > 0) {
                Flags.getInstance().unset(Flags.ZERO);
                Flags.getInstance().set(Flags.CARRY);
            } else {
                Flags.getInstance().unset(Flags.ZERO);
            }
        }
    }

    void storeValue(int address) {
        if (address >= 0) {
            Memory.getInstance().write(address, read());
        }
    }

    void storeValue(int address, Registers register) {
        if (address >= 0 && register != null) {
            int offset;
            if (Registers.X.equals(register)) {
                offset = XRegister.getInstance().read().intValue();
            } else {
                offset = YRegister.getInstance().read().intValue();
            }
            Memory.getInstance().write(address + offset, read());
        }
    }

    BigInteger read() {
        return this.value != null ? value : BigInteger.ZERO;
    }

 }
