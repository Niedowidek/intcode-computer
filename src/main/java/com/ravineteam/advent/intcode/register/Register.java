package com.ravineteam.advent.intcode.register;

import java.math.BigInteger;

public abstract class Register {

    private BigInteger value = BigInteger.ZERO;

    public BigInteger getValue() {
        return value;
    }

    void increase() {
        this.value = this.value.add(BigInteger.ONE);
    }

    void decrease() {
        this.value = this.value.subtract(BigInteger.ONE);
    }

    BigInteger read() {
        return value != null ? value : BigInteger.ZERO;
    }

    void write(BigInteger arg) {
        if (arg != null) {
            this.value = BigInteger.ZERO.add(arg);
        }
    }

    void storeValue(int address) {
        if (address > 0) {
            Memory.getInstance().write(address, read());
        }
    }

    void loadValue(int address) {
        if (address > 0) {
            write(Memory.getInstance().read(address));
        }
    }


    void compare(int address) {

    }

    void compare(BigInteger arg) {

    }

}
