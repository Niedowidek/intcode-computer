package com.ravineteam.advent.intcode.register;

import java.math.BigInteger;

public class AccFacadeExtended {
    private static AccFacadeExtended ourInstance = new AccFacadeExtended();

    public static AccFacadeExtended getInstance() {
        return ourInstance;
    }

    private AccFacadeExtended() {
    }

    public void multiply(int address) {
        if (address > 0) {
            BigInteger bg = BigInteger.ZERO.add(Memory.getInstance().read(address));
            multiply(bg);
        }
    }

    public void multiply(BigInteger arg) {
        if (arg != null) {
            BigInteger multipliedValue = Accumulator.getInstance().read();
            for (BigInteger i = BigInteger.ONE; i.compareTo(arg) != 0; i = i.add(BigInteger.ONE)) {
                Accumulator.getInstance().add(multipliedValue);
            }
        }
    }
}
