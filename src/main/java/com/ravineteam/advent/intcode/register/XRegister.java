package com.ravineteam.advent.intcode.register;

import java.math.BigInteger;

public class XRegister extends Register {
    private static XRegister ourInstance = new XRegister();

    static XRegister getInstance() {
        return ourInstance;
    }

    private XRegister() {
    }

    void loadValue(int address, Registers register) {
        if (address >= 0 && Registers.Y.equals(register)) {
            this.loadValue(address + YRegister.getInstance().read().intValue());
        }
    }
}
