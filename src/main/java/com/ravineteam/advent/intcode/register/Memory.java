package com.ravineteam.advent.intcode.register;

import java.math.BigInteger;

public class Memory {
    private static Memory ourInstance = new Memory();
    private BigInteger[] memory;

    public static Memory getInstance() {
        return ourInstance;
    }

    private Memory() {
        this.memory = new BigInteger[65535];
    }

    void write(int address, BigInteger value) {
        if (address >= 0 && address < memory.length && value != null) {
            this.memory[address] = BigInteger.ZERO.add(value);
        }
    }

    BigInteger read(int address) {
        if (address >= 0 && address < memory.length && this.memory[address] != null) {
            return BigInteger.ZERO.add(this.memory[address]);
        }
        return BigInteger.ZERO;
    }
}
