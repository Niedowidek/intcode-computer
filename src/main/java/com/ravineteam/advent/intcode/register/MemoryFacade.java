package com.ravineteam.advent.intcode.register;

import java.math.BigInteger;

public class MemoryFacade {
    private static MemoryFacade ourInstance = new MemoryFacade();

    public static MemoryFacade getInstance() {
        return ourInstance;
    }

    private MemoryFacade() {
    }

    public BigInteger peek(int address) {
        if (address > 0) {
            return Memory.getInstance().read(address);
        }
        return BigInteger.ZERO;
    }
}
