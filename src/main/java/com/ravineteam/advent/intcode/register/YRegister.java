package com.ravineteam.advent.intcode.register;

public class YRegister extends Register {
    private static YRegister ourInstance = new YRegister();

    static YRegister getInstance() {
        return ourInstance;
    }

    private YRegister() {
    }

    void loadValue(int address, Registers register) {
        if (address >= 0 && Registers.X.equals(register)) {
            this.loadValue(address + XRegister.getInstance().read().intValue());
        }
    }
}
