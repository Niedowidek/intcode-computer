package com.ravineteam.advent.intcode.operation;

import com.ravineteam.advent.intcode.branch.Invokable;
import com.ravineteam.advent.intcode.register.*;

import java.lang.reflect.Method;
import java.math.BigInteger;

public class Opcode {

    public void adc(int address){
        AccFacade.getInstance().adc(address);
    }

    public void adc(BigInteger arg){
        AccFacade.getInstance().adc(arg);
    }

    public void and(){
        //TODO
    }

    public void asl(){
        //TODO
    }

    public void bcc(){
        //TODO
    }

    public void bcs(){
        //TODO
    }

    public void beq(Invokable invokable){
        Branch.getInstance().equal(invokable);
    }

    public void bit(){
        //TODO
    }

    public void bmi(){
        //TODO
    }

    public void bne(Invokable invokable){
        Branch.getInstance().notEqual(invokable);
    }

    public void bpl(){
        //TODO
    }

    public void brk(){
        //TODO
    }

    public void bvc(){
        //TODO
    }

    public void bvs(){
        //TODO
    }

    public void clc(){
        //TODO
    }

    public void cld(){
        //TODO
    }

    public void cli(){
        //TODO
    }

    public void clv(){
        //TODO
    }

    public void cmp(BigInteger arg){
        AccFacade.getInstance().cmp(arg);
    }

    public void cpx(){
        //TODO
    }

    public void cpy(){
        //TODO
    }

    public void dec(){
        //TODO
    }

    public void dex(){
        RegisterOpsFacade.getInstance().dex();
    }

    public void dey(){
        RegisterOpsFacade.getInstance().dey();
    }

    public void eor(){
        //TODO
    }

    public void inc(){
        //TODO
    }

    public void inx(){
        RegisterOpsFacade.getInstance().inx();
    }

    public void iny(){
        RegisterOpsFacade.getInstance().iny();
    }

    public void jmp(Invokable invokable){
        Jump.getInstance().jmp(invokable);
    }

    public void jsr(){
        //TODO
    }

    public void lda(BigInteger arg){
        AccFacade.getInstance().lda(arg);
    }

    public void lda(int address){
        AccFacade.getInstance().lda(address);
    }

    public void lda(int address, Registers register){
        AccFacade.getInstance().lda(address, register);
    }

    public void ldx(int address){
        RegisterOpsFacade.getInstance().ldx(address);
    }

    public void ldx(int address, Registers register){
        RegisterOpsFacade.getInstance().ldx(address, register);
    }

    public void ldx(BigInteger arg){
        RegisterOpsFacade.getInstance().ldx(arg);
    }

    public void ldy(int address){
        RegisterOpsFacade.getInstance().ldy(address);
    }

    public void ldy(int address, Registers register){
        RegisterOpsFacade.getInstance().ldy(address, register);
    }

    public void ldy(BigInteger arg){
        RegisterOpsFacade.getInstance().ldy(arg);
    }

    public void lsr(){
        //TODO
    }

    public void nop(){
        //TODO
    }

    public void ora(){
        //TODO
    }

    public void pha(){
        //TODO
    }

    public void php(){
        //TODO
    }

    public void pla(){
        //TODO
    }

    public void plp(){
        //TODO
    }

    public void rol(){
        //TODO
    }

    public void ror(){
        //TODO
    }

    public void rti(){
        //TODO
    }
    public void rts(){
        //TODO
    }

    public void sbc(){
        //TODO
    }

    public void sec(){
        //TODO
    }

    public void sed(){
        //TODO
    }

    public void sei(){
        //TODO
    }

    public void sta(int address){
        AccFacade.getInstance().sta(address);
    }

    public void sta(int address, Registers register){
        AccFacade.getInstance().sta(address, register);
    }

    public void stx(int address){
        RegisterOpsFacade.getInstance().stx(address);
    }

    public void sty(int address){
        RegisterOpsFacade.getInstance().sty(address);
    }

    public void tax(){
        TransferFacade.getInstance().tax();
    }

    public void tay(){
        TransferFacade.getInstance().tay();
    }

    public void tsx(){
        //TODO
    }

    public void txa(){
        TransferFacade.getInstance().txa();
    }

    public void txs(){
        //TODO
    }

    public void tya(){
        TransferFacade.getInstance().tya();
    }

}
