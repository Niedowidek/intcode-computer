package com.ravineteam.advent.intcode.operation;


import com.ravineteam.advent.intcode.register.AccFacade;
import com.ravineteam.advent.intcode.register.AccFacadeExtended;
import com.ravineteam.advent.intcode.register.MemoryFacade;

import java.math.BigInteger;

public class OpcodeExtended extends Opcode {

    public BigInteger peek(int address) {
        return MemoryFacade.getInstance().peek(address);
    }

    public void multiply(int address) {
        AccFacadeExtended.getInstance().multiply(address);
    }

    public void multiply(BigInteger arg) {
        AccFacadeExtended.getInstance().multiply(arg);
    }

}
