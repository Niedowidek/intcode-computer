package com.ravineteam.advent.intcode.branch;

public interface Invokable {

    void invoke();
}
